﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FileManager.Controllers
{
    public class HomeController : Controller
    {
        string apiLink = Properties.Settings.Default.api + "LINK";
        public ActionResult Index()
        {
            ViewBag.ApiLink = apiLink;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.ApiLink = apiLink;
            ViewBag.aaa = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaA";
            ViewBag.bbb = "bbBB";

            return View((object)apiLink);
        }

        public ActionResult AddFile()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult OAuth()
        {
            if (!User.Identity.IsAuthenticated)
            {
                // ce nastavimo 401, bo cookie middleware redirect-al na LoginPath, Response.StatusCode = 401;      // ali  
                //HttpContext.Current.GetOwinContext().Authentication.Challenge(DefaultAuthenticationTypes.ApplicationCookie);
            }
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.ApiLink = apiLink;
            ViewBag.Message = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            return View();
        }
    }
}