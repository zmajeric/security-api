﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using SecurityAPI.Models;
using System.Security.Claims;

namespace SecurityAPI.Controllers
{
    public class FileController : ApiController
    {
        public IEnumerable<SecurityAPI.Models.FileModel> Get() {
            var identity = User.Identity as ClaimsIdentity;
            List<SecurityAPI.Models.FileModel> files = new List<SecurityAPI.Models.FileModel>();
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.conn);
            conn.Open();
            int userId = 2;

            SqlCommand command = new SqlCommand("Select * FROM [Files]", conn);
            //SqlCommand command = new SqlCommand("Select id from [table1] where name=@zip", conn);
            //command.Parameters.AddWithValue("@zip", "india");
            // int result = command.ExecuteNonQuery();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    files.Add(new SecurityAPI.Models.FileModel {
                        Id = Convert.ToInt32(reader["Id"]),
                        FileName = "" + reader["File_name"],
                        FilePath = "\\Users\\" + userId  + "\\" + reader["File_name"],
                        Created = DateTime.Parse(Convert.ToString(reader["Date_time"])),
                        Size = Convert.ToInt32(reader["Size"]),
                        Owner = Convert.ToInt32(reader["Owner"])
                    });
                }
            }

            conn.Close();
            return files;
        }
        [Authorize]
        [Route("api/File/Add")]
        public async Task<HttpResponseMessage> PostFormData()
        {

            if (!User.Identity.IsAuthenticated)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            string uname = User.Identity.Name;
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.conn);
            SqlCommand uId = new SqlCommand("SELECT Id from [dbo].[Users] WHERE username = @uname", conn);
            uId.Parameters.AddWithValue("@uname", uname);
            conn.Open();
                int userId = Convert.ToInt32(uId.ExecuteScalar());
            string path = "~/Users/"+ userId + "/";

            bool exists = Directory.Exists(HttpContext.Current.Server.MapPath(path));

            if (!exists)
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
            string root = HttpContext.Current.Server.MapPath(path);

            var provider = new ExtendedMultipartFormData(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);
                String fileName = "Test.jpg";
                /* 
                 * Show all the key-value pairs.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        Trace.WriteLine(string.Format("{0}: {1}", key, val));
                    }
                }
                
                    INSERT INTO dbo.Files("Id", "File_name","Date_time" ,"Size" ) VALUES (1,'test.jpg','10/10/2010', 33);
                    INSERT INTO dbo.Users("Id", "username","password" ,"ime","priimek","gender" ) VALUES (1,'test','test', 'zan','majeric','Male');
                    INSERT INTO dbo.Files_Users_mm("Id","fk_file","fk_user") VALUES(1,1,1);
                 */

                
                int modified = 0;
                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    SqlCommand insert = new SqlCommand("INSERT INTO dbo.Files(File_name,Date_time ,Size,Owner ) OUTPUT INSERTED.ID VALUES (@FileName, @DateTime, @Size,@Owner)", conn);
                    insert.Parameters.AddWithValue("@FileName", file.Headers.ContentDisposition.FileName.Replace("\"",""));
                    insert.Parameters.AddWithValue("@DateTime", DateTime.Now);
                    insert.Parameters.AddWithValue("@Size", 33);
                    insert.Parameters.AddWithValue("@Owner", userId);
                    modified = Convert.ToInt32(insert.ExecuteScalar());

                    if (conn.State == System.Data.ConnectionState.Open)
                        conn.Close();
                }
                if (modified > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else {
                    return Request.CreateResponse(HttpStatusCode.Conflict);
                }
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        
        [Authorize]
        [Route("api/File/{id}")]
        public HttpResponseMessage GetFile(int id)
        {
            if (User.Identity.IsAuthenticated) { 
                string uname = User.Identity.Name;
                SqlConnection conn = new SqlConnection(Properties.Settings.Default.conn);

                SqlCommand command = new SqlCommand("Select * FROM [dbo].[Files] AS Files LEFT JOIN [dbo].[Users] AS Users" +
                    " ON Files.Owner = Users.Id WHERE Users.Username = @uname AND Files.Id = @fId", conn);
                command.Parameters.AddWithValue("@uname", uname);
                command.Parameters.AddWithValue("@fId", id);
                //SqlCommand command = new SqlCommand("Select id from [table1] where name=@zip", conn);
                //command.Parameters.AddWithValue("@zip", "india");
                // int result = command.ExecuteNonQuery();
                string fileName = "";
                conn.Open();

                SqlCommand uId = new SqlCommand("SELECT Id from [dbo].[Users] WHERE username = @uname", conn);
                uId.Parameters.AddWithValue("@uname", uname);
                int userId = Convert.ToInt32(uId.ExecuteScalar());
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        fileName = ""+reader["File_name"];
                    }
                }
                // TODO: filename check

                conn.Close();
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Users/"+ userId + "/"+fileName); ;
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = Path.GetFileName(path);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = stream.Length;
                return result;
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
    

    public class ExtendedMultipartFormData : MultipartFormDataStreamProvider
    {
        string fileName = "default.jpg";
        public ExtendedMultipartFormData(string path) : base(path) { }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            return headers.ContentDisposition.FileName.Replace("\"", string.Empty);
        }
        
    }
}
