﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace SecurityAPI.Controllers
{
    public class AuthController : ApiController
    {
        [HttpPost]
        public async System.Threading.Tasks.Task<IHttpActionResult> PostAuthAsync(HttpRequestMessage request)
        {
            NameValueCollection formData = await request.Content.ReadAsFormDataAsync();
            ClaimsIdentity ci = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            ci.AddClaim(new Claim(ClaimTypes.Name,(string) formData["username"]));
            HttpContext.Current.GetOwinContext().Authentication.SignIn(ci);
            string parameters = "?response_type=token&client_id="+formData["client_id"]+"&redirect_uri=" + formData["redirect_uri"]+ "&state=mystate";
            return Redirect("http://localhost:42407/OAuth.aspx" + parameters);
        }

    }
}
