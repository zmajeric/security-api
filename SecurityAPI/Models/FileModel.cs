﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityAPI.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime Created { get; set; }
        public int Size { get; set; }
        public int Owner { get; set; }
        
    }
}