﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityAPI.Models
{
    public class RefreshToken
    {
        public string hashedRefreshToken { get; set; }
        public string user { get; set; }
        public string client_id { get; set; }
        public DateTimeOffset? issuedUTC { get; set; }
        public DateTimeOffset? expiresUTC { get; set; }
        public string ticket { get; set; }
    }
}