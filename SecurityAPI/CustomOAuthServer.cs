﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using System.Security.Principal;
using System.Web.Security;
using System.Data.SqlClient;

namespace SecurityAPI
{
    class CustomAuthServer : OAuthAuthorizationServerProvider
    {
        private bool success;

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            { 
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            //var grant_type = context.Parameters["grant_type"];

            SqlConnection conn = new SqlConnection(Properties.Settings.Default.conn);
            SqlCommand command = new SqlCommand("Select * FROM [ClientData] WHERE [ClientData].clientId = @cId AND [ClientData].clientSecret = @cSecret ", conn);
            command.Parameters.AddWithValue("@cId", clientId);
            command.Parameters.AddWithValue("@cSecret", clientSecret);
            conn.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                if(reader.HasRows)
                    context.Validated();
                else
                    context.SetError("invalid_clientId", "Client or secret is wrong.");
            }
            conn.Close();
            context.OwinContext.Set<string>("as:clientAllowedOrigin", "*");
            context.OwinContext.Set<int>("as:clientRefreshTokenLifeTime", 1800);
            context.OwinContext.Set<string>("as:client_id", context.ClientId);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            // grant_type=password
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin") ?? "*";

            // s tem headerjem lahko omejimo, da bi kdo iz svoje JavaScript aplikacije, klical naš API
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            // TODO dobimo uporabnika iz baze
            // TODO clienta preverimo iz baze
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.conn);
            SqlCommand command = new SqlCommand("Select * FROM [Users] WHERE Username = @uname", conn);
            command.Parameters.AddWithValue("@uname", context.UserName);
            conn.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        if ((string)reader["Password"] == context.Password)
                        {
                            success = true;

                            /*
                            List<Claim> claims = new List<Claim>();
                            claims.Add(new Claim(ClaimTypes.Name, context.UserName));
                            claims.Add(new Claim(ClaimTypes.Role, "admin"));
                            claims.Add(new Claim("as:client_id", context.ClientId));*/
                            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                            identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                            identity.AddClaim(new Claim("as:client_id", context.ClientId));


                            if (!string.IsNullOrEmpty(context.Scope.FirstOrDefault()))
                                identity.AddClaims(context.Scope.First()
                                                                ?.Split(',')
                                                                ?.Select(t => new Claim("as:scope", t)));


                            var props = new AuthenticationProperties(new Dictionary<string, string> {
                                {"client_id", context.ClientId},
                                {"username", context.UserName}
                            });

                            var ticket = new AuthenticationTicket(identity, props);

                            context.Validated(ticket);
                        }
                    }
                    if (!success)
                    {
                        context.Rejected();
                        context.SetError("invalid_grant", "The user name or password is incorrect.");

                    }
                }
                else
                {
                    context.Rejected();
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                }
            }
            conn.Close();

        }

        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
        }

        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            // grant_type = refresh_token

            // refresh token lahko uporabi samo tisti client, ki ga je dobil
            // (dobil ga je ta, ki ga preberemo iz baze in je v Ticket.Properties)
            // trenutnega clienta iz contexta in njegov secret preverimo v ValidateClientAuthentication
            var originalClient = context.Ticket.Properties.Dictionary["client_id"];
            var currentClient = context.ClientId;
            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return;
            }

            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            // to lahko spremenimo, uprabnik je mogoče dobil drugačno vlogo (role) ali pa ima novo telefonsko, ali kaj dodatnega
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            // za to metodo se pokliče CreateAsync, tako da uporabnik dobi tudi novi refresh token
        }

        public override async Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            //  "grant_type" of "client_credentials".
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("as:client_id", context.ClientId));
            context.Validated(identity);
        }

        public override async Task AuthorizeEndpoint(OAuthAuthorizeEndpointContext context)
        {
            context.RequestCompleted();
        }

        public override async Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            // response_type = code
            
            ClaimsIdentity ci = new ClaimsIdentity("Bearer");
            ci.AddClaim(new Claim(ClaimTypes.Name, context.OwinContext.Authentication.User.Identities.ElementAt(0).Name));
            context.OwinContext.Authentication.SignIn(ci);

            SqlConnection conn = new SqlConnection(Properties.Settings.Default.conn);
            SqlCommand command = new SqlCommand("Select * FROM [ClientData] WHERE [ClientData].clientId = @cId ", conn);
            command.Parameters.AddWithValue("@cId", context.ClientId);
            conn.Open();
            int r = command.ExecuteNonQuery();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows) {
                    var redirectUri = context.RedirectUri;
                    context.OwinContext.Set<string>("as:user", context.OwinContext.Authentication.AuthenticationResponseGrant.Identity.Name);
                    context.Validated();
                }
                else
                    context.SetError("invalid_clientId", "Client or secret is wrong.");
            }
            conn.Close();
            
        }
        
    }
}