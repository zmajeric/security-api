﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OAuth.aspx.cs" Inherits="SecurityAPI.OAuth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>
            Permissions requested
        </h1>
        <sub>Application requires following permissions: </sub>
        <asp:CheckBox ID="perm_write" runat="server" Text="Write" />
        <asp:CheckBox ID="perm_read" runat="server" Text="Read" />
        <asp:CheckBox ID="perm_user_info" runat="server" Text="User info" />
        <asp:Button runat="server" ID="Confirm" Text="CONFIRM" OnClick="Confirm_Click" />
    </form>
</body>
</html>
