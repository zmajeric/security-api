﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using Microsoft.Owin.Security.Cookies;
using System.Web.Http.Cors;

[assembly: OwinStartup(typeof(SecurityAPI.Startup))]
namespace SecurityAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
#if DEBUG
                AllowInsecureHttp = true,
#endif
                TokenEndpointPath = new PathString("/api/token"),
                AuthorizeEndpointPath = new PathString("/api/code"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                AuthorizationCodeExpireTimeSpan = TimeSpan.FromMinutes(30),
                Provider = new CustomAuthServer(),
                RefreshTokenProvider = new RefreshTokenProvider()
            };

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Login.aspx"),
                ExpireTimeSpan = TimeSpan.FromMinutes(30),
            });

            
            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());


            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            var cors = new EnableCorsAttribute(
            origins: "*",
            headers: "*",
            methods: "*");
            config.EnableCors(cors);
            WebApiConfig.Register(config);

            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }
    }
}