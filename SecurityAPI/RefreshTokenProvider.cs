﻿using Microsoft.Owin.Security.Infrastructure;
using SecurityAPI.Models;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SecurityAPI
{
    public class RefreshTokenProvider : AuthenticationTokenProvider {
        public static readonly ConcurrentDictionary<string, RefreshToken> refreshTokens = new ConcurrentDictionary<string, RefreshToken>();

        public override async Task CreateAsync(AuthenticationTokenCreateContext context)
    {
        var clientid = context.Ticket.Properties.Dictionary["client_id"];
        var user = context.Ticket.Properties.Dictionary["username"];

        // to smo nastavili, ko smo validirali clienta, saj je lahko za vsakega clienta drugače
        var refreshTokenLifeTime = context.OwinContext.Get<int>("as:clientRefreshTokenLifeTime");

        context.Ticket.Properties.IssuedUtc = DateTime.UtcNow;
        context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddSeconds(refreshTokenLifeTime);

        RefreshToken rt = new RefreshToken
        {
            client_id = clientid,
            user = user,
            issuedUTC = context.Ticket.Properties.IssuedUtc,
            expiresUTC = context.Ticket.Properties.ExpiresUtc,
        };

        // ustvarimo global unique identifier GUID (refresh token)         
        var refreshToken = Guid.NewGuid().ToString("n") + Guid.NewGuid().ToString("n");

        rt.hashedRefreshToken = Helper.GetHash(refreshToken);
        rt.ticket = context.SerializeTicket();

        refreshTokens.TryAdd(rt.hashedRefreshToken, rt);
        context.SetToken(refreshToken);
    }

    public override async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
    {
        // ta metoda se pokliče za grant_type=refresh_token
        var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin") ?? "*";
        context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

        string refreshToken = context.Token;

        RefreshToken rt;

        if (refreshTokens.TryRemove(Helper.GetHash(refreshToken), out rt))
        {
            if (rt.expiresUTC > DateTime.UtcNow)
                context.DeserializeTicket(rt.ticket);
        }
    }
}
}