﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityAPI
{
    public class AuthContext: IdentityDbContext
    {
        public AuthContext()
            : base("AuthContext")
        {

        }
    }
}