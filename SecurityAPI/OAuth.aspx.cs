﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SecurityAPI
{
    public partial class OAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                // ce nastavimo 401, bo cookie middleware redirect-al na LoginPath, Response.StatusCode = 401;      // ali  
                HttpContext.Current.GetOwinContext().Authentication.Challenge(DefaultAuthenticationTypes.ApplicationCookie);
            }
        }

        protected void Confirm_Click(object sender, EventArgs e)
        {
            // tu bi mogel iti dejansko na api/token a mi je zmanjkalo časa
            var redirectStr = "api/code" + Request.Url.Query+"&scope=";

            if (perm_write.Checked)
            {
                redirectStr += "write";
            }
            if (perm_read.Checked)
            {
                redirectStr += ",read";
            }
            if (perm_user_info.Checked)
            {

                redirectStr += ",user_info";
            }
            Server.TransferRequest(redirectStr);
        }
    }
}